import 'package:flutter/material.dart';
import 'package:clipboard/clipboard.dart';
import 'package:pwgen/password.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Password Generator',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Password Generator'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _checkCapitals = true;
  bool _checkNumbers = false;
  bool _checkSymbols = false;
  int _passwordLengthNumber = 16;
  String _passwordLengthString = "16";
  var _controller = TextEditingController();

  void _getRandomPassword() {
    setState(() {
      _controller.text = Password().random(
          _passwordLengthNumber, _checkCapitals, _checkNumbers, _checkSymbols);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 25),
                  Text(
                    "Options",
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  CheckboxListTile(
                    title: Text("Include capital letters in password"),
                    value: _checkCapitals,
                    onChanged: (newValue) {
                      setState(() {
                        _checkCapitals = newValue;
                      });
                    },
                    controlAffinity: ListTileControlAffinity
                        .leading, //  <-- leading Checkbox
                  ),
                  CheckboxListTile(
                    title: Text("Include numbers in password"),
                    value: _checkNumbers,
                    onChanged: (newValue) {
                      setState(() {
                        _checkNumbers = newValue;
                      });
                    },
                    controlAffinity: ListTileControlAffinity
                        .leading, //  <-- leading Checkbox
                  ),
                  CheckboxListTile(
                    title: Text("Include symbols in password"),
                    value: _checkSymbols,
                    onChanged: (newValue) {
                      setState(() {
                        _checkSymbols = newValue;
                      });
                    },
                    controlAffinity: ListTileControlAffinity
                        .leading, //  <-- leading Checkbox
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 25),
                  Text(
                    "Length ",
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  Column(children: [
                    Slider(
                        min: 4,
                        max: 64,
                        value: _passwordLengthNumber.toDouble(),
                        onChanged: (double newValue) {
                          setState(() {
                            _passwordLengthNumber = newValue.toInt();
                            _passwordLengthString =
                                _passwordLengthNumber.toString();
                          });
                        }),
                    Text(_passwordLengthString),
                  ]),
                ],
              ),
              Column(children: [
                SizedBox(height: 50),
                new Container(
                  margin: const EdgeInsets.all(20.0),
                  child: SizedBox(
                      width: double.infinity,
                      child: ElevatedButton.icon(
                        style: Theme.of(context).textButtonTheme.style,
                        onPressed: () {
                          _getRandomPassword();
                        },
                        label: Text("Generate",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold)),
                        icon: Icon(
                          Icons.play_arrow,
                          color: Colors.white,
                        ),
                      )),
                ),
                SizedBox(height: 50),
              ]),
              new Container(
                margin: const EdgeInsets.all(20.0),
                child: SizedBox(
                    width: double.infinity,
                    child: TextField(
                      readOnly: true,
                      controller: _controller,
                      decoration: InputDecoration(
                        hintText: "",
                        suffixIcon: IconButton(
                          onPressed: () =>
                              FlutterClipboard.copy(_controller.text),
                          icon: Icon(Icons.copy),
                        ),
                      ),
                    )),
              ),
            ])));
  }
}
