import 'dart:math';

class Password {

  static const String _alphaChars = "abcdefghijklmnopqrstuvwxyz";
  static const String _alphaCapsChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  static const String _numeralChars = "0123456789";
  static const String _symbolChars = "!#%&/()=?_-+*,.<>";

  var rng = new Random();


  String random(int passwordLength, bool useCapitalizedChars, bool useNumeralChars, bool useSymbolChars) {

    int numberOfCharsRemaining = passwordLength;
    int numberOfCharsCapitalized;
    int numberOfCharsNumerals;
    int numberOfCharsSymbols;
    List<String> charList = new List();

    if (useCapitalizedChars) {
      double tmp = passwordLength * 0.3;
      numberOfCharsCapitalized = tmp.floor();
      numberOfCharsRemaining -= numberOfCharsCapitalized;
      for(int i = 0; i < numberOfCharsCapitalized; i++) {
        charList.add('A');
      }
    }

    if (useNumeralChars) {
      double tmp = passwordLength * 0.2;
      numberOfCharsNumerals = tmp.floor();
      numberOfCharsRemaining -= numberOfCharsNumerals;
      for(int i = 0; i < numberOfCharsNumerals; i++) {
        charList.add('N');
      }
    }

    if (useSymbolChars) {
      double tmp = passwordLength * 0.2;
      numberOfCharsSymbols = tmp.floor();
      numberOfCharsRemaining -= numberOfCharsSymbols;
      for(int i = 0; i < numberOfCharsSymbols; i++) {
        charList.add('S');
      }
    }

    for(int i = 0; i < numberOfCharsRemaining; i++) {
      charList.add('a');
    }

    charList.shuffle(rng);

    StringBuffer sb = new StringBuffer();
    charList.forEach((ch) {
      switch(ch) {
        case 'a':
          sb.write(_getRandomChar(_alphaChars));
          break;
        case 'A':
          sb.write(_getRandomChar(_alphaCapsChars));
          break;
        case 'N':
          sb.write(_getRandomChar(_numeralChars));
          break;
        case 'S':
          sb.write(_getRandomChar(_symbolChars));
          break;
      }
    });

    return sb.toString();
  }


  String _getRandomChar(String str) {
    var randomInt = rng.nextInt(str.length);
    return str.substring(randomInt, randomInt+1);
  }


}