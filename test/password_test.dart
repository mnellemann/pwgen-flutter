import 'package:test/test.dart';
import 'package:pwgen/password.dart';

void main() {
  test('Length of random password', () {
    var randomPassword = Password().random(8, false, false, false);
    expect(randomPassword.length, 8);
  });
}
